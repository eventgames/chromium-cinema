rem Copy the git binary outside the source controlled files, so we can use it to also update GIT itself
cd C:\EventGamesBrowser
copy /y portable_git\cmd\git_git.exe portable_git\cmd\git.exe
xcopy portable_git\mingw32\bin_git portable_git\mingw32\bin /s /y /i

