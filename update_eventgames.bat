rem Update the Eventgames runtime
set gitdir=c:\EventGamesBrowser\portable_git
set path=%gitdir%\cmd;%path%
cd c:\EventGamesBrowser

rem Remove read-only attribute from preferences, to allow updating
attrib -R Default\Preferences

git pull

rem Add read-only attribute to preferences to avoid Chromium changing the settings
attrib +R Default\Preferences

rem Ensure GIT is functional
enable_git
